require('./lib/mongoose-connection');
require('passport');
var redis = require('./lib/redis-connection');
var express = require("express");
var path = require('path');
var bodyParser = require('body-parser');
var pdf = require('pdfcrowd');
var client = new pdf.Pdfcrowd('thomasdavis', '7d2352eade77858f102032829a2ac64e');
var app = express();
var request = require('superagent');
var expressSession = require('express-session');
var cookieParser = require('cookie-parser');
var compress = require('compression');
var minify = require('express-minify');
var controller = require('./controller');
var permission = require('permission');
var methodOverride = require('method-override');
var fs = require('fs');
var RedisStore = require('connect-redis')(expressSession);

var points = [];
var DEFAULT_THEME = 'modern';
var notAuthenticated = {
  redirect: '/login'
};
var port = Number(process.env.PORT || 9000);

app.use(compress());
app.use(minify({
    cache: __dirname + '/cache'
}));
app.use(methodOverride('_method'));
app.use(require('./middleware/allow-cross-domain'));
app.use(cookieParser());
app.use(expressSession({
    store: new RedisStore({
        client: redis
    }),
    secret: 'keyboard cat',
    resave: true,
    saveUninitialized: true
}));
app.use('/assets', express.static(__dirname + '/assets', {}));
app.use(function(req, res, next){
  // if(!req.session.user_id.length && !req.originalUrl.indexOf('/login')){
  //   res.redirect('/login');
  // } else {
    next();
  // }
});
app.set('permission', {
  role: 'role',
  notAuthenticated: notAuthenticated,
  notAuthorized: notAuthenticated,
});
app.use(bodyParser());
app.use(function (req, res, next){
  req.isAuthenticated = function(){
    return this.session.user_id ? true : false;
  };
  req.user = {role: req.session.role || 'user'};
  next();
});

app.all('/*', function(req, res, next) {
    //res.header("Access-Control-Allow-Origin", "*");
    //res.header("Access-Control-Allow-Headers", "X-Requested-With");

    // Make the db accessible to the router
    // probably not the most performant way to pass the db's around
    // TODO find a better way
    req.redis = redis;
    next();
});

app.get('/', controller.homeController)
// BASIC ACTIONS FOR SIGNIN AND SIGNUP AND LOGOUT
app.get('/register', controller.registrationController);
app.get('/login', controller.loginController);
// USERS ACTIONS
app.post('/users', controller.usersController.userCreate);
app.get('/users/:id', controller.usersController.userShow);
app.get('/users/:id/edit', require('permission')(['admin', 'user']), controller.usersController.userEdit);
app.put('/users/:id', require('permission')(['admin', 'user']), controller.usersController.userUpdate);
app.get('/users/:id/delete', require('permission')(['admin']), controller.usersController.userDelete);
// RESUMES ACTIONS
app.get('/resumes/new', controller.resumesController.newResume);
app.get('/resumes/:id', controller.resumesController.showResume);
app.post('/resumes', controller.resumesController.createResume);
app.get('/resumes/:id/edit', controller.resumesController.editResume);
app.put('/resumes', controller.resumesController.updateResume);
app.delete('/resumes/:id', require('permission')(['admin', 'user']), controller.resumesController.deleteResume);
app.get('/resumes/:id/find', controller.resumesController.findResume);
app.get('/admin', require('permission')(['admin']), controller.adminController.adminActions);
app.get('/admin/resumes/generate-uniq-link/:resume_id', require('permission')(['admin']), controller.adminController.generateUniqLink);
app.delete('/admin/resumes/remove-uniq-link/:resume_id', require('permission')(['admin']), controller.adminController.removeUniqLink)
app.post('/resumes/generate/:template', controller.adminController.generateResumeTemplate)
app.get('/:guid', controller.resumesController.showResumeByGuid);
app.post('/session', controller.sessionsController.createSession);
app.get('/session/delete', controller.sessionsController.deleteSession);



app.use(function(err, req, res, next) {
  console.error(err.stack);
  // res.status(500).send('Something broke!');
});
// app.get('/session', controller.checkSession);
// app.delete('/session/:id', controller.deleteSession);
// app.get('/members', controller.renderMembersPage);
// app.get('/stats', controller.showStats);
// export pdf route
// this code is used by resume-cli for pdf export, see line ~188 for web-based export
// app.get('/pdf', function(req, res) {
//     // console.log(req.body.resume, req.body.theme);
//     request
//         .post('https://themes.jsonresume.org/theme/' + req.body.theme)
//         .send({
//             resume: req.body.resume
//         })
//         .set('Content-Type', 'application/json')
//         .end(function(err, response) {
//             client.convertHtml(response.text, pdf.sendHttpResponse(res), {
//                 use_print_media: "true"
//             });
//         });
// });

// app.get('/:uid.:format', controller.renderResume);
// app.get('/:uid', controller.renderResume);
// app.post('/resume', controller.upsertResume);
// app.put('/resume', controller.updateTheme);
// app.put('/account', controller.changePassword);
// app.delete('/account', controller.deleteUser);
// app.post('/:uid', controller.renderResume);

process.addListener('uncaughtException', function(err) {
    console.error('Uncaught error in server.js', {
        err: err
        // hide stack in production
        //, stack: err.stack
    });
    // TODO some sort of notification
    // process.exit(1);
});


app.listen(port, function() {
    console.log("Listening on " + port);
});

module.exports = app;
module.exports.DEFAULT_THEME = DEFAULT_THEME;
