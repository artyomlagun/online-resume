set :deploy_to, '/home/app/itexus-resume'
server '192.168.1.30:62999', user: 'app', roles: [:web, :app, :db], primary: true

set :ssh_options, {
  keys: %w(~/.ssh/id_rsa)
}

set :use_sudo, false
set :branch, 'master'

set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'node_modules', 'assets/resumes')

set :pm2_app_command, 'server.js'                   # default, runs main.js
set :pm2_app_name, 'itexus-resumes'                            # app name for pm2, default cap :application
set :pm2_target_path, -> { release_path } # default not set
set :pm2_roles, :all                              # default, cap roles to run on
set :pm2_env_variables, {'NODE_ENV' => 'production'}                        # default, env vars for pm2
set :pm2_start_params, '-e /home/app/itexus-resume/current/log/err.log -o /home/app/itexus-resume/current/log/out.log -p /home/app/itexus-resume/current/tmp/pids/itexus_resumes.pid'

set :npm_target_path, -> { release_path } # default not set
set :npm_flags, '--production --silent --no-spin' # default
set :npm_roles, :all                              # default
set :npm_env_variables, {}
