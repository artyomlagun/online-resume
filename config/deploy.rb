# config valid only for current version of Capistrano
lock '3.6.1'

set :application, 'itexus-resumes'
set :repo_url, 'git@bitbucket.org:artyomlagun/online-resume.git'

set :default_stage, 'production'
set :stages, %w(production staging)

set :scm, :git
set :deploy_via, :remote_cache

set :keep_releases, 5
set :shared_children, %w()

set :pty, true

set :linked_files, %w()
set :linked_dirs, %w()

namespace :deploy do
  desc 'start node server'
  task :start_server do
    on roles(:app) do
      within current_path do
        execute 'cd #{current_path} && pm2 stop all && pm2 delete all && pm2 start pm2.json'
      end
    end
  end
end
after 'deploy:publishing', 'deploy:start_server'




# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
# set :deploy_to, '/var/www/my_app_name'

# Default value for :scm is :git
# set :scm, :git

# Default value for :format is :airbrussh.
# set :format, :airbrussh

# You can configure the Airbrussh format using :format_options.
# These are the defaults.
# set :format_options, command_output: true, log_file: 'log/capistrano.log', color: :auto, truncate: :auto

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# append :linked_files, 'config/database.yml', 'config/secrets.yml'

# Default value for linked_dirs is []
# append :linked_dirs, 'log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'public/system'

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5
