var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var resumeSchema = new Schema({
  user_id: {
    type: String,
    required: true,
  },
  theme: {
    type: String,
  },
  resume_body: {
    type: Schema.Types.Mixed,
    required: true,
  },
  updated_at: {
    type: Date,
    required: true,
    default: Date.now,
  },
  visibility: {
    type: String,
    required: true,
  },
  guids: {
    type: [String],
  },
});

module.exports = mongoose.model('Resume', resumeSchema);
