// Global by intention.
var builder;

function reset() {
	$.getJSON("/assets/json/resume.json", function(data) {
		builder.setFormValues(data);
	});
};

function clear() {
	builder.setFormValues({});
};

function enableTSEplugin() {
	var preview = $("#preview");
	var scrollable = $(".tse-scrollable");
	scrollable.TrackpadScrollEmulator();
	scrollable.on("startDrag", function() {
		preview.addClass("scroll");
	});
	scrollable.on("endDrag", function() {
		preview.removeClass("scroll");
	});
};

function enableCSStransitions() {
	setTimeout(function() {
		$("body").removeClass("preload");
	}, 200);
};

$(document).ready(function() {
  $('.tabs-list a').click(function (e) {
    e.preventDefault()
    $(this).tab('show')
  })

	var form = $("#form");
	builder = new Builder(form);

  $.getJSON("/assets/json/schema.json", function(data) {
    builder.init(data);
    if($('#action').val() == 'new'){
        reset();
    } else if($('#action').val() == 'edit'){
      var id = $('#resume_id').val();
      $.getJSON('/resumes/'+id+'/find', function(resume){
        builder.setFormValues(resume.resume.resume_body);
      });
    };
  });

	var preview = $("#preview");
	var iframe = $("#iframe");

	(function() {
		var timer = null;
		form.on("change", function() {
			clearTimeout(timer);
			preview.addClass("loading");
			timer = setTimeout(function() {
				var data = builder.getFormValues();
				form.data("resume", data);
				postResume(data);
			}, 200);
		});
	})();

  $('body').on('click', '#save', function(e){
    e.preventDefault();
    var resumeData = dataForSaveResume();
    $.post('/resumes', resumeData, function(data){
      console.log(data);
      if(data.status == 'successfull'){
        window.location.href = '/users/' + data.user;
      } else {
        $('#error-block').html(data.error.message);
      }
    })
  });
  $('body').on('click', '#save_edit', function(e){
    e.preventDefault();
    var resumeData = dataForSaveResume();
    $.ajax({
      url: '/resumes',
      method: 'PUT',
      data: resumeData,
      success: function(data){
        if(data.status == 'successfull'){
          window.location.href = '/';
        } else {
          $('#sidebar').prependTo(data.error);
          $(data.error.message).prependTo('#sidebar')
        }
      }
    });
  });

  function dataForSaveResume(){
    var data = builder.getFormValues();
    var visibility = $('#resume_visibility').val();
    var theme = $('#resume_theme').val();
    var userId = $('#_id').val();
    var hash = window.location.hash;
    if (hash != "") {
      theme = hash.replace("#", "");
    }
    var resumeId = '';
    if($('#action').val() == 'edit'){
      resumeId = $('#resume_id').val();
    }
    var resumeData = {
      user_id: userId,
      resume_body: data,
      theme: theme,
      visibility: visibility,
      resume_id: resumeId,
    };
    return resumeData;
  }

  $('#export').click(function(e){
    e.preventDefault();
    var styles = $('#iframe').contents().find('body link');
    $.when(getThemeStyles(styles)).then(function(data){
      var args = Array.prototype.slice.call(arguments, 0);
      // console.log(args, data)
      $('#iframe').contents().find('body').prepend('<style>' + args.join(' ') + '</style>');
      styles.remove();
      var resume = $('#iframe').contents().find('html').html();
      $.ajax({
        method: 'POST',
        url: '/resumes/generate/pdf',
        data: {htmlResume: resume},
        success: function(data){
          $('#editor').prepend('<a href='+data+' target="_blank" id="fake_link">Generated link to PDF</a>');
          // var win1 = window.open(data, '_blank');
          // win1.focus();
          $('#fake_link').trigger('click');
        }
      });
    })
  });

  function getThemeStyles(styles){
    var arrayAjax = new Array();
    $.each(styles, function(i, item){
      arrayAjax.push($.ajax({
        method: 'GET',
        url: $(item).href
      }).then(function(data){
        console.log(data.responseText)
        return data.responseText;
      }));
    });

    return arrayAjax;
  };

	function postResume(data) {
		var theme = "flat";
		var hash = window.location.hash;
		if (hash != "") {
			theme = hash.replace("#", "");
		}
		$.ajax({
			type: "POST",
			contentType: "application/json",
			data: JSON.stringify({resume: data}, null, "  "),
			url: "https://themes.jsonresume.org/" + theme,
			success: function(html) {
				iframe.contents().find("body").html(html);
				preview.removeClass("loading");
			}
		});
		(function toggleActive() {
			$("#theme-current").html(theme);
			var active = $("#themes-list .item[href='#" + theme + "']").addClass("active");
			active.siblings().removeClass("active");
		})();
	}

	enableTSEplugin();
	enableCSStransitions();

	// $("#export").on("click", function() {
	// 	var data = form.data("resume");
	// 	download(JSON.stringify(data, null, "  "), "resume.json", "text/plain");
	// });
	$("#export, #save, #clear, #reset, .uniq-link").tooltip({
		container: "body"
	});


	$("#reset").on("click", function() {
		if (confirm("Are you sure?")) {
			reset();
		}
	});

	$("#clear").on("click", function() {
		if (confirm("Are you sure?")) {
			clear();
		}
	});

	var tabs = $("#sidebar .tabs a");
	tabs.on("click", function() {
		var self = $(this);
		self.addClass("active").siblings().removeClass("active");
	});

	(function getThemes() {
		var list = $("#themes-list");
		var item = list.find(".item").remove();
		$.getJSON("https://themes.jsonresume.org/themes.json", function(json) {
			var themes = json.themes;
			if (!themes) {
				return;
			}
			for (var t in themes) {
				var theme = item
					.clone()
					.attr("href", "#" + t)
					.find(".name")
					.html(t)
					.end()
					.find(".version")
					.html(themes[t].versions.shift())
					.end()
					.appendTo(list);
			}
		});
		list.on("click", ".item", function() {
			form.trigger("change");
		});
	})();

	var jsonEditor = $("#json-editor");

	(function() {
		var timer = null;
		jsonEditor.on("keyup", function() {
			clearTimeout(timer);
			timer = setTimeout(function() {
				try {
					var text = jsonEditor.val();
					builder.setFormValues(JSON.parse(text))
				} catch(e) {
					// ..
				}
			}, 200);
		});
	})();

	form.on("change", function() {
		var json = builder.getFormValuesAsJSON();
		if (jsonEditor.val() !== json) {
			jsonEditor.val(json);
		}
	});

	$("#sidebar .view").on("click", "a", function(e) {
		e.preventDefault();
		var self = $(this);
		var type = self.data("type");
		self.addClass("active").siblings().removeClass("active");
		jsonEditor.toggleClass("show", type == "json");
	});
});
