$(document).ready(function(){

  $('.delete-resume').on('click', function(e){
    e.preventDefault();
    var choosedResume = $(e.target).parents('li');
    var resumeId = $(e.target).data('id');
    var url = '/resumes/' + resumeId;
    deleteItem(url, choosedResume, 'Resume was deleted successful');
  });

  $('.delete-guid').on('click', function(e){
    e.preventDefault();
    var choosedGuid = $(e.target).parent();
    var url = $(e.target).attr('href');
    deleteItem(url, choosedGuid, 'GUID was deleted successful')
  });

  function deleteItem(url, choosedItem, statusText){
    if(confirm('Are you sure?')){
      $.ajax({
        type: 'DELETE',
        url: url
      }).success(function(data){
        if(data.status == statusText){
          $(choosedItem).remove();
        }
      })
    }
  }

  new Clipboard('.uniq-link');

});
