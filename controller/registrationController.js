var fs = require('fs');
var templateHelper = require('../template-helper');
var Mustache = require('mustache');

module.exports = function registrationController(req, res, next) {
  var content = Mustache.render(templateHelper.get('register'));
  var page = Mustache.render(templateHelper.get('layout'), {content: content});
  res.send(page);
}
