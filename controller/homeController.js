var fs = require('fs');
var templateHelper = require('../template-helper');
var Mustache = require('mustache');

module.exports = function homeController(req, res, next) {
  if(req.session.user_id){
    res.redirect('/users/' + req.session.user_id);
  } else {
    res.redirect('/login');
  }
}
