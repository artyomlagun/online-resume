var fs = require('fs');
var bcrypt = require('bcrypt-nodejs');
var templateHelper = require('../template-helper');
var Mustache = require('mustache');
var User = require('../models/user');
var Resume = require('../models/resume');

module.exports.userCreate = function userCreate(req, res, next) {
  User.findOne({
    'email': req.body.email
  }, function(err, user) {
    if (user) {
      res.status(409).json({ // HTTP Status 409 Conflict
        error: {
          field: 'email',
          message: 'Email is already in use, maybe you forgot your password?'
        }
      });
    } else {
      User.findOne({
        'username': req.body.username
      }, function(err, user) {
        if (user) {
          res.status(409).json({ // HTTP Status 409 Conflict
            error: {
              field: 'username',
              message: 'This username is already taken, please try another one'
            }
          });
        } else {
          var newUser = userParameters(req)
          User.create(newUser, function(err, user) {
            if (err) {
              return next(err);
            }
            // req.session.username = user.username;
            // req.session.email = user.email;
            // req.session.id = user._id;
            res.redirect('/admin')
          });
        }
      });
    };
  });
};

module.exports.userShow = function userShow(req, res, next){
  User.findOne({_id: req.params.id}, function(err, user){
    if(user){
      if(user.role == 'admin'){
        res.redirect('/admin');
      } else {
        var resumes = Resume.find({user_id: user._id, visibility: 'user'}, function(err, resume){
          if(resume){
            var content = Mustache.render(templateHelper.get('userShow'), {user: user, resumes: resume});
            var page = Mustache.render(templateHelper.get('layout'), {content: content});
            res.send(page);
          }
        });
      }
    } else {
      res.status(404).json({
        error: {
          message: 'User not found!'
        }
      });
    }
  });
};

module.exports.userEdit = function userEdit(req, res, next){
  User.findOne({_id: req.params.id}, function(err, user){
    if(err){
      console.log('BITCH')
      res.status(409).json({
        error: {
          message: "User not found!"
        }
      });
    }
    var isUserAdmin = user.role == 'admin';
    var content = Mustache.render(templateHelper.get('userEdit'), {user: user, isAdmin: isUserAdmin});
    var page = Mustache.render(templateHelper.get('layout'), {content: content});
    res.send(page);
  });
};

module.exports.userUpdate = function userUpdate(req, res, next){
  var userParams = userParameters(req);
  User.findOneAndUpdate({_id: req.params.id}, {$set: userParams}, function(err, user){
    if(err){
      res.status(409).json({
        error: {
          message: 'Update was fault: ' + err
        }
      });
    }
    res.redirect('/');
  });
};

module.exports.userDelete = function userDelete(req, res, next){
  User.remove({_id: req.params.id}, function(err){
    if(err){
      res.status(409).json({
        error: {
          message: "User wasn't delete!"
        }
      })
    } else {
      res.redirect("/admin");
    }
  });
};

var userParameters = function(req){
  var hash;
  var userObject;
  var role = req.body.role != null ? req.body.role : 'user';
  if(req.body.password != ''){
    hash = bcrypt.hashSync(req.body.password);
    userObject = {
      username: req.body.username,
      email: req.body.email,
      hash: hash,
      role: role,
    }
  } else {
    userObject = {
      username: req.body.username,
      email: req.body.email,
      role: role,
    }
  }
  return userObject;
}
