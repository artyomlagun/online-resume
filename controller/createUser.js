var fs = require('fs');
var bcrypt = require('bcrypt-nodejs');
var Mustache = require('mustache');
var User = require('../models/user');

module.exports = function userController(req, res, next) {
  console.log('hit the user controller');
  User.findOne({
    'email': req.body.email
  }, function(err, user) {
    if (user) {
      res.status(409).json({ // HTTP Status 409 Conflict
        error: {
          field: 'email',
          message: 'Email is already in use, maybe you forgot your password?'
        }
      });
    } else {
      User.findOne({
        'username': req.body.username
      }, function(err, user) {
        if (user) {
          res.status(409).json({ // HTTP Status 409 Conflict
            error: {
              field: 'username',
              message: 'This username is already taken, please try another one'
            }
          });
        } else {
          var hash = bcrypt.hashSync(req.body.password);
          var newUser = {
            username: req.body.username,
            email: req.body.email,
            password: req.body.password,
            hash: hash,
            role: 'user',
          };
          User.create(newUser, function(err, user) {
            if (err) {
              return next(err);
            }
            req.session.username = user.username;
            req.session.email = user.email;
            res.redirect('/users/'+user.id)
          });
        }
      });
    };
  });
};
