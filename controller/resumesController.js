var fs = require('fs');
var templateHelper = require('../template-helper');
var Mustache = require('mustache');
var User = require('../models/user');
var Resume = require('../models/resume');

function renderTemplate(contentName, options, pageName){

}

module.exports.createResume = function createResume(req, res, next){
  var visibility = req.body.visibility !== 'undefined' ? req.body.visibility : 'user';
  var newResume = {
    user_id: req.body.user_id,
    resume_body: req.body.resume_body,
    theme: req.body.theme,
    updated_at: new Date(),
    visibility: visibility,
  };
  Resume.create(newResume, function(err, resume){
    if(err){
      return res.json({status: 'failed', error: err})
    }
    res.status(200).json({status: 'successfull', user: req.session.user_id});
  });
};

module.exports.newResume = function newResume(req, res, next){
  var user;
  var isAdmin = req.session.role == 'admin' ? true : false;

  if(typeof req.query.user_id !== 'undefined'){
    user = req.query.user_id;
  } else {
    user = req.session.user_id;
  }
  var content = Mustache.render(templateHelper.get('newResume'), {user: user, isAdmin: isAdmin});
  var page = Mustache.render(templateHelper.get('resumeLayout'), {content: content});
  res.send(page);
};

module.exports.showResume = function showResume(req, res, next){
  Resume.findOne({_id: req.params.id}, function(err, resume){
    if(resume){
      var content = Mustache.render(templateHelper.get('showResume'), {resume: JSON.stringify(resume.resume_body), theme: resume.theme});
      var page = Mustache.render(templateHelper.get('layout'), {content: content});
      res.send(page);
    } else {
      res.status(409).json({
        error: {
          message: 'Resume not found!'
        }
      });
    };
  });
};

module.exports.editResume = function editResume(req, res, next){
  var isAdmin = req.session.role == 'admin' ? true : false;
  Resume.findOne({_id: req.params.id}, function(err, resume){
    if(resume){
      var content = Mustache.render(templateHelper.get('editResume'), {resume: resume, user: req.session.user_id, isAdmin: isAdmin});
      var page = Mustache.render(templateHelper.get('resumeLayout'), {content: content});
      res.send(page);
    } else {
      res.status(404).json({
        error: {
          message: 'Resume not found!'
        }
      });
    };
  });
};

module.exports.updateResume = function updateResume(req, res, next){
  var visibility = typeof req.body.visibility !== 'undefined' ? req.body.visibility : 'user';
  var resumeParams = {
    theme: req.body.theme,
    resume_body: req.body.resume_body,
    updated_at: new Date(),
    visibility: visibility,
  };
  User.findOne({_id: req.body.user_id}, function(err, user){
    if(user){
      Resume.findOneAndUpdate({_id: req.body.resume_id}, {$set: resumeParams}, function(err, resume){
        if(err){
          return res.json({status: 'failed', error: err})
        }
        res.json({status: 'successfull', user: user});
      });
    }
  })

  // res.redirect('/users/' + req.session.user_id)
};

module.exports.findResume = function findResume(req, res, next){
  Resume.findOne({_id: req.params.id}, function(err, resume){
    if(resume){
      res.status(200).json({resume: resume});
    } else {
      res.status(409).json({
        error: {
          message: 'Resume not found!'
        }
      });
    }
  });
};

module.exports.showResumeByGuid = function showResumeByGuid(req, res, next){
  Resume.findOne({guids: req.params.guid}, function(err, resume){
    if(resume){
      var content = Mustache.render(templateHelper.get('showResume'), {resume: JSON.stringify(resume.resume_body), theme: resume.theme});
      var page = Mustache.render(templateHelper.get('layout'), {content: content});
      res.send(page);
    } else {
      res.status(409).json({
        error: {
          message: 'Resume not found!'
        }
      });
    };
  });
};

module.exports.deleteResume = function deleteResume(req, res, next){
  Resume.findOneAndRemove({_id: req.params.id}, function(err, resume){
    if(err){
      return res.status(409).json({status: 'failed', error: err})
    }
    res.status(200).json({status: 'Resume was deleted successful'})
  });
};
