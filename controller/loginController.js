var fs = require('fs');
var templateHelper = require('../template-helper');
var Mustache = require('mustache');

module.exports = function loginController(req, res, next) {
  if(req.session.user_id){
    res.redirect('/users/' + req.session.user_id);
  } else {
    var content = Mustache.render(templateHelper.get('login'));
    var page = Mustache.render(templateHelper.get('layout'), {content: content});
    res.send(page);
  }
};
//
// module.exports = function logoutUser(req, res, next) {
//
// };
