var fs = require('fs');
var templateHelper = require('../template-helper');
var Mustache = require('mustache');
var User = require('../models/user');
var Resume = require('../models/resume');
var pdf = require('html-pdf');
var _ = require('lodash');
var lodashArray = require('lodash/array')
var options = {
  format: 'A4',
  border: '0',
  zoomFactor: '1',
  type: 'pdf',
}
function s4() {
  return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
};

function processDate(updatedAt){
  var d = new Date(updatedAt);
  var day = d.getDate();
  var month = d.getMonth() + 1;
  var year = d.getFullYear();
  var processedDate = day + '-' + month + '-' + year;
  return processedDate;
}

var findUsers = function(){
  return new Promise(function(resolve, reject){
    User.find({}, function(err, users){
      if(err){
        reject(err);
      }
      if(users){
        resolve(users.sort({username: 'asc'}));
      } else {
        resolve([]);
      }
    });
  });
};

var findUserResume = function(users){
  var usersPromiseCollection = [];
  users.forEach(function(user, index){
    usersPromiseCollection.push(new Promise(function(resolve, reject){
      Resume.find({user_id: user._id}, function(err, resumes){
        if(err){
          reject(err);
        }
        var resumesDoubleArray = _.partition(resumes, r => r.visibility == 'user');
        var latestUpdatedResume = _.last(resumes);
        var latestUpdate = '';
        if(latestUpdatedResume !== undefined){
          latestUpdate = processDate(latestUpdatedResume.updated_at)
        }
        resolve({user: user, index: 'user_' + index, userResumes: resumesDoubleArray[0], adminResumes: resumesDoubleArray[1], latestUpdate: latestUpdate});
      });
    }));
  });
  return Promise.all(usersPromiseCollection);
};
// var usersList = function usersList(){
//   return new Promise(function(resolve, reject){
//     User.find({}, function(err, users){
//       if(err){
//         reject(err);
//       }
//       resolve(users.sort({username: 'asc'}));
//     });
//   });
// };

var allResumesPromised = function allResumesPromised(){
  return findUsers().then(function(users){
    return findUserResume(users);
  });
};

var resumeToPdf = function resumeToPdf(html, response){
  var tempName = new Date().getTime();
  options['border'] = {top: '2cm', right: '1.5cm', bottom: '2cm', left: '2.5cm'};
  return pdf.create(html, options).toFile('assets/resumes/' + tempName + '.pdf', function(err, res){
    if(err){
      console.log(err);
    }
    response.send('/assets/resumes/' + tempName + '.pdf');
  });
};
var resumeToHtml = function resumeToHtml(html, response){

};

module.exports.adminActions = function adminActions(req, res, next){
  var usersResumesOriginal = allResumesPromised().then(function(data){
    // console.log(_.first(_.first(data).userResumes).resumeguid);
    var content = Mustache.render(templateHelper.get('admin'), {users: data});
    var page = Mustache.render(templateHelper.get('layout'), {content: content});
    res.send(page);
  }).catch(function(e){
    console.log(e);
  });
};

module.exports.generateUniqLink = function generateUniqLink(req, res, next){
  var uniqLink = s4() + s4() + s4() + s4();
  Resume.findOne({_id: req.params.resume_id}, function(err, resume){
    if(err){
      res.status(409).json({
        error: {
          message: 'GUID generated was fault: ' + err
        }
      });
    }
    resume.guids.push(uniqLink);
    resume.save(function(err, resume){
      res.redirect('/admin');
    });
  });
};
module.exports.removeUniqLink = function removeUniqLink(req, res, next){
  Resume.findOne({_id: req.params.resume_id}, function(err, resume){
    if(err){
      res.status(409).json({
        error: {
          message: 'Resume not found: ' + err
        }
      });
    }
    console.log('original guids: ', resume.guids);
    var guids = _.pull(resume.guids, req.query.uniq_link);
    console.log('final guids: ', guids);
    resume.update({$set: {guids: guids}}, function(err, resume){
      if(err){
        res.status(409).json({
          error: {
            message: 'GUID removed was fault: ' + err
          }
        });
      }
      res.status(200).json({status: 'GUID was deleted successful'});
    });
  });
};

module.exports.generateResumeTemplate = function generateResumeTemplate(req, res, next){
  switch(req.params.template){
    case 'pdf':
      resumeToPdf(req.body.htmlResume, res);
      break;
    case 'html':

      break;
    default:

      break;
  };
};
