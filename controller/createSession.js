var HttpStatus = require('http-status-codes');
var bcrypt = require('bcrypt-nodejs');
var User = require('../models/user');

module.exports = function createSession(req, res, next) {

    var redis = req.redis

    function uid(len) {
        return Math.random().toString(35).substr(2, len);
    }

    var password = req.body.password;
    var email = req.body.email;
    // console.log(req.body);
    User.findOne({
        'email': email
    }, function(err, user) {
        
        if (user && bcrypt.compareSync(password, user.hash)) {
            var sessionUID = uid(32);

            redis.set(sessionUID, true, redis.print);

            req.session.username = user.username;
            req.session.email = email;
            req.session.user_id = user._id;
            req.session.role = user.role;
            res.redirect('/users/' + user._id);
            // redis.quit();
        } else {
            res.status(HttpStatus.UNAUTHORIZED).send({
                message: 'authentication error'
            });
        }
    });
};
